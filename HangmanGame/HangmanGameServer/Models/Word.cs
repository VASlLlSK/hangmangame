﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HangmanGameServer.Models
{
    public class Word
    {
        public int id { get; set; }
        public string word { get; set; }
    }
}