﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using Dapper;
using System.Dynamic;
using Newtonsoft.Json;

namespace HangmanGameServer.Models
{
    public class DataBase
    {
        string _connectionString = "Server=localhost;Port=3306;Database=hangman;Uid=root;Pwd=54321ASASAS;";
        private MySqlConnection connection;
        public DataBase()
        {
            connection = new MySqlConnection(_connectionString);
        }

        public string GetWordFromSomeTable(string table, string Uname)
        {
            Word word = null;

            var sql = "SELECT id, word FROM hangman." + table + " as t,";
            sql += "(SELECT ROUND((SELECT MAX(id) FROM hangman." + table + ") * rand()) as rnd ";
            sql += "FROM hangman." + table + " LIMIT 1) tmp ";
            sql += "WHERE id in (rnd) ";
            sql += "ORDER BY word";

            word = connection.Query<Word>(sql).First();


            sql = "SELECT PassedWords FROM hangman.users WHERE Name = @Uname";
            var passedWordsJSON = connection.Query<string>(sql, new { Uname = Uname }).First();

            PassedWords passedWords = JsonConvert.DeserializeObject<PassedWords>(passedWordsJSON);

            var passedIDFromSomeTable = passedWords.getArrayFromTable(table);

            if (!passedIDFromSomeTable.Contains(word.id))
            {
                passedIDFromSomeTable.Add(word.id);
            }
            else
            {
                var str = "";
                foreach (var strID in passedIDFromSomeTable)
                {
                    str += strID.ToString() + ", ";
                }
                str = str.Remove(str.Length - 2, 2);
                sql = "SELECT id,word FROM hangman." + table + " WHERE id NOT IN (" + str + ")";
                word = connection.Query<Word>(sql).FirstOrDefault();
                if (word == null || word.word == "")
                    return null;
                passedIDFromSomeTable.Add(word.id);
            }

            string json = JsonConvert.SerializeObject(passedWords);

            sql = "UPDATE hangman.users SET PassedWords = @PassedWords WHERE Name = @Uname";

            connection.Query<string>(sql, new { Uname = Uname, PassedWords = json });

            if (word != null && word.word.Contains('-'))
            {
                word.word = word.word.Replace("-", "");
            }

            return word.word;
        }


        public string GetSpesialWord(SettingsForSearch settings)
        {

            var sql = "SELECT word FROM ( SELECT word FROM hangman.nouns WHERE wcase = \"им\"";
            sql += settings.GetAlifePartQuery(true);
            sql += settings.GetBeginEndPartQuery(true);
            sql += settings.GetGenderPartQuery(true);
            sql += settings.GetLengthPartQuery(true);
            sql += ") as t ORDER BY rand() LIMIT 1";

            var word = connection.Query<string>(sql).FirstOrDefault();

            if (word != null && word.Contains('-'))
            {
                word = word.Replace("-", "");
            }

            return word;
        }

        public List<User> GetRatingTop10()
        {
            var sql = "SELECT Name,Score FROM hangman.users ORDER BY Score DESC, Score DESC LIMIT 10";
            var result = connection.Query<User>(sql).ToList();
            return result;
        }


        public string Login(User u)
        {
            var sql = "SELECT id,Name,Password FROM hangman.users WHERE Name = @name";
            var result = connection.Query<User>(sql, new { name = u.Name }).FirstOrDefault();

            if (result == null)
                return "Неверный  пароль";

            if (result.Password == u.Password)
                return "";
            return "Неверный  пароль";
        }

        public string Registration(User u)
        {
            if (u.Name == "")
                return "Заполните  поле  NAME";

            if (u.Password == "")
                return "Заполните  поле  PASSWORD";

            var sql = "SELECT id,Name FROM hangman.users WHERE Name = @Name";
            var result = connection.Query<User>(sql, new { Name = u.Name }).FirstOrDefault();

            if (result == null)
            {
                sql = "INSERT INTO hangman.users (Name, Password, PassedWords, Score) VALUES (@Name, @Password, @PassedWords, @Score)";
                connection.Query<User>(sql, new { Name = u.Name, Password = u.Password, PassedWords = PassedWords.GetStartedJsonPassedWord(), Score = 0 });
                return "ВЫ успешно прошли регистрацию!";
            }

            return "Это  имя  занято";
        }

        public void UdateScore(User u)
        {
            var sql = "SELECT Score FROM hangman.users WHERE Name = @Name";
            var Score = connection.Query<int>(sql, new { Name = u.Name }).FirstOrDefault();

            if (Score < u.Score)
            {
                sql = "UPDATE hangman.users SET Score = @Score WHERE Name = @name";
                connection.Query<int>(sql, new { name = u.Name, Score = u.Score }).FirstOrDefault();
            }
        }

        public void ClearPassedWord(User u)
        {
            var sql = "UPDATE hangman.users SET PassedWords = @PassedWords WHERE Name = @name";
            connection.Query<int>(sql, new { name = u.Name, PassedWords = PassedWords.GetStartedJsonPassedWord() }).FirstOrDefault();
        }

    }

}