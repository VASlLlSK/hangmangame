﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using HangmanGameServer.Models;

namespace HangmanServer.Controllers
{
    public class UserController : ApiController
    {
        private DataBase db;
        public UserController()
        {
            db = new DataBase();
        }

        // GET api/RaitingTop10
        [Route("api/RatingTop10")]
        public async Task<List<User>> Get()
        {
            return await Task.Run(() => {
                return db.GetRatingTop10();
            });
        }

        // POST api/Login
        [Route("api/Login")]
        public async Task<string> Post([FromBody]User u)
        {
            return await Task.Run(() => {
                return db.Login(u);
            });
        }

        // POST api/Registration
        [Route("api/Registration")]
        public async Task<string> PostR([FromBody]User u)
        {
            return await Task.Run(() => {
                return db.Registration(u);
            });
        }

        // PUT api/UpdateScore
        [Route("api/UpdateScore")]
        public async void Put([FromBody]User u)
        {
            await Task.Run(() => {
                db.UdateScore(u);
            });
        }

    }
}
