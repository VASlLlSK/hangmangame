﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using HangmanGameServer.Models;

namespace HangmanServer.Controllers
{
    public class WordsController : ApiController
    {
        private DataBase db;
        public WordsController()
        {
            db = new DataBase();
        }

        // GET api/Words
        [Route("api/Words")]
        public async Task<string> Get([FromUri] SettingsForSearch settings)
        {
            return await Task.Run(() => {
                return db.GetSpesialWord(settings);
            });
        }

        // GET api/Words/table
        [Route("api/Words/{Uname}/{table}")]
        public async Task<string> Get(string table, string Uname)
        {
            return await Task.Run(() => {
                return db.GetWordFromSomeTable(table, Uname);
            });
        }

        // Put api/Words/table
        [Route("api/ClearPassedWord")]
        public async void Put([FromBody]User u)
        {
            await Task.Run(() => {
                db.ClearPassedWord(u);
            });
        }
    }
}
