﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlTypes;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using EasyHttp.Http;
using Newtonsoft.Json;

namespace HangmanGame
{
    public class NPC : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    class MainViewModel : NPC
    {
        char[] currentWordChar;
        string currentWordString;
        string secretWord;
        string tip;
        string error;
        string currentTable;
        string currentTableRus;
        string _hangmanIMG;
        int fiftyfifty = 0;
        int ors = 0;
        bool buttonsleep = false;
        public string CurrentWord { get { return currentWordString; } set { currentWordString = value; OnPropertyChanged(); } }
        public string TIP { get { return tip; } set { tip = value; OnPropertyChanged(); } }
        public string hangmanIMG { get { return _hangmanIMG; } set { _hangmanIMG = value; OnPropertyChanged(); } }
        public string CurrentTableRus { get { return currentTableRus; } set { currentTableRus = value; OnPropertyChanged(); } }
        public string Error { get { return error; } set { error = value; OnPropertyChanged(); } }

        MainWindow ui;

        Task _task;
        public List<myChar> Alphabet { get; set; }
        public CurrentUser CUser { get; set; }
        public MyCategory myCategory { get; set; }
        public ObservableCollection<User> Rating { get; set; }
        public Stack<DockPanel> Page { get; set; }
        public MyMediaPlayer MMP { get; set; }

        public delegate void NextPrimeDelegate();
        public delegate void NextPrimeDelegateUsers(User[] str);

        public MainViewModel(MainWindow mainWindow)
        {
            Alphabet  = new List<myChar>();
            Rating = new ObservableCollection<User>();
            Page = new Stack<DockPanel>();
            CUser = new CurrentUser();
            myCategory = new MyCategory();
            MMP = new MyMediaPlayer();
            secretWord = "";
            currentTable = "";
            TIP = "";
            ui = mainWindow;

            SetAlfabet();
            currentWordChar = new char[secretWord.Length * 2];
            CurrentWord = new string(currentWordChar);
        }

        public void SetAlfabet()
        {
            for (int i = 'А', j = 0; i <= 'Я'; i++)
            {
                var temp = new myChar();
                temp.c = (char)i;
                temp.X = (i - 'А' + j) / 11;
                temp.Y = (i - 'А' + j) % 11;
                temp.visibility = Visibility.Visible;
                Alphabet .Add(temp);
                if (i == 1045)
                {
                    j = 1;
                    temp = new myChar();
                    temp.c = (char)1025;
                    temp.X = 0;
                    temp.Y = 6;
                    temp.visibility = Visibility.Visible;
                    Alphabet .Add(temp);
                }
            }
        }

        public void openChar(char c)
        {
            for (int i = 0; i < secretWord.Length; i++)
            {
                if (secretWord[i] == c)
                {
                    currentWordChar[i * 2] = c;
                }
            }
            CurrentWord = new string(currentWordChar);
        }

        public void GetNewWord(object o)
        {
            buttonsleep = true;
            var client = new HttpClient();
            string url = "https://localhost:44335/api/Words";
            string table = (string)o;

            if (table == "customCategory")
            {
                url += "?Alife=" + myCategory.Alife;
                url += "&Length=" + myCategory.Length;
                url += "&OpenSymbol=" + myCategory.OpenSymbol;
                url += "&BeginVowel=" + myCategory.BeginVowel;
                url += "&BeginConsonant=" + myCategory.BeginConsonant;
                url += "&EndVowel=" + myCategory.EndVowel;
                url += "&EndConsonant=" + myCategory.EndConsonant;
                url += "&Man=" + myCategory.Man;
                url += "&Woman=" + myCategory.Woman;
                url += "&Neuter=" + myCategory.Neuter;
            }
            else
                url += "/" + CUser.Name + "/" + table;

            var response = client.Get(url);
            var wordFromServer = response.StaticBody<string>();
            if (wordFromServer == null)
            {
                wordFromServer = "перейдите в след. категорию!";
                CurrentWord = wordFromServer;
                buttonsleep = true;
            }
            else
            {
                secretWord = wordFromServer.ToUpper();
                currentWordChar = new char[secretWord.Length * 2];
                for (int i = 0; i < secretWord.Length * 2; i++)
                {
                    currentWordChar[i] = '_';
                    i++;
                    currentWordChar[i] = ' ';
                }
                CurrentWord = new string(currentWordChar);

                if (table == "customCategory")
                {
                    for (int i = 0; i < myCategory.OpenSymbol; i++)
                    {
                        ui.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                                        new NextPrimeDelegate(ORS));
                    }
                }

                buttonsleep = false;
            }
        }

        private void ORS()
        {
            var rnd = new Random();
            for (; ; )
            {
                var j = rnd.Next() % secretWord.Length;
                if (currentWordChar[j * 2] == '_')
                {
                    openChar(secretWord[j]);
                    var someChar = Alphabet .Find(X => X.c == secretWord[j]);
                    someChar.visibility = Visibility.Hidden;
                    break;
                }
                if (secretWord.Equals(CurrentWord.ToString().Replace(" ", "")))
                    break;
            }
        }

        public void Rules()
        {
            if (secretWord.Equals(CurrentWord.ToString().Replace(" ", "")) || CUser.Life == 0)
            {
                Task.Run(() =>
                {
                    buttonsleep = true;
                    CurrentWord = secretWord;
                    Thread.Sleep(2500);

                    SaveScore();
                    GetNewWord(currentTable);
                    hangmanIMG = "Resources/Images/1.png";
                    buttonsleep = false;
                });

                if (CUser.Life == 0)
                {
                    CUser.Score = 0;
                    ors = 0;
                    fiftyfifty = 0;
                    MMP.loseSound();
                    ClearPassedWord();
                }
                else
                {
                    CUser.Score++;
                    if (ors != 0)
                        ors++;
                    if (fiftyfifty != 0)
                        fiftyfifty++;
                    MMP.winSound();
                }
                CUser.Life = 7;
                OpenAllAlfabetSymbols();
            }
        }

        public void OpenAllAlfabetSymbols()
        {
            Alphabet .ForEach(delegate (myChar symbol)
            {
                symbol.visibility = Visibility.Visible;
            });
        }
        public void HiddenAllAlfabetSymbols()
        {
            Alphabet .ForEach(delegate (myChar symbol)
            {
                symbol.visibility = Visibility.Hidden;
            });
        }
        public void VisibleSomeDockPanel(DockPanel ui)
        {
            ui.Visibility = Visibility.Visible;
        }
        public void CollapsedSomeDockPanel(DockPanel ui)
        {
            ui.Visibility = Visibility.Collapsed;
        }
        public void FillTIPforCategory()
        {
            TIP = "Иникальные слова будут выдоваться до тех пор пока вы не проиграете. После проигрыша ваши достижения будут обновленны.\n";
            TIP += "В категории Хардкор собраны все слова из словаря, в именительном падеже.\n";
            TIP += "В категории \"Свой режим\" вы можете настроить под себя характиристики выдоваемых слов.\n\n";
            TIP += "ПРИМЕЧАНИЕ: В режиме \"Свой режим\" могут попадаться слова которые вы уже отгадывали. В этом режиме ваши достижения не будут сохранены.";
        }
        public void FillTIPforGame()
        {
            TIP = "МАЛЕНЬКИЕ ХИТРОСТИ:\n";
            TIP += "• Слово легче отгадывать, когда известны согласные, входящие в него.\n";
            TIP += "• Чем длиннее слово, тем проще его угадать.\n";
            TIP += "• Начинать отгадывать имеет смысл с наиболее чаще встречающихся букв: о, е, и, а,...\n";
            TIP += "ПОДСКАЗКИ:\n";
            TIP += "• 50/50 - убирает 50% букв. После использования подсказки она будет перезаряжаться 3 хода\n";
            TIP += "• ORS - открывает случайную букву в слове. После использования подсказки она будет перезаряжаться 2 хода\n";
        }
        public void SaveScore()
        {
            if (currentTable != "customCategory")
            {
                var client = new HttpClient();
                string url = "https://localhost:44335/api/UpdateScore";
                string data = JsonConvert.SerializeObject(CUser);
                var response = client.Put(url, data, HttpContentTypes.ApplicationJson);
            }
        }
        public void ClearPassedWord()
        {
            var client = new HttpClient();
            string url = "https://localhost:44335/api/ClearPassedWord";
            string data = JsonConvert.SerializeObject(CUser);
            var response = client.Put(url, data, HttpContentTypes.ApplicationJson);
        }



        private RelayCommand _UIpreviousPage;
        public RelayCommand UIPreviousPage
        {
            get
            {
                return _UIpreviousPage ??
                  (_UIpreviousPage = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      var someDockPanel = Page.Pop();
                      CollapsedSomeDockPanel(someDockPanel);
                      someDockPanel = Page.Peek();
                      VisibleSomeDockPanel(someDockPanel);

                      switch (someDockPanel.Name)
                      {
                          case "MENU":
                              {
                                  ui.BAR.Visibility = Visibility.Collapsed;
                                  break;
                              }
                          case "CATEGORIES":
                              {
                                  ui.TIP.Visibility = Visibility.Visible;
                                  ui.NEXT.Visibility = Visibility.Collapsed;
                                  ui.GAMEBAR.Visibility = Visibility.Hidden;
                                  FillTIPforCategory();
                                  break;
                              }
                          case "CUSTOM":
                              {
                                  ui.GAMEBAR.Visibility = Visibility.Hidden;
                                  ui.NEXT.Visibility = Visibility.Visible;
                                  ui.TIP.Visibility = Visibility.Collapsed;
                                  break;
                              }
                          case "GAME":
                              {
                                  ui.GAMEBAR.Visibility = Visibility.Visible;
                                  ui.NEXT.Visibility = Visibility.Collapsed;
                                  ui.TIP.Visibility = Visibility.Visible;
                                  break;
                              }
                          default: break;
                      }
                  }, x => {
                      if (Page.Count <= 1)
                          return false;
                      return true;
                  }));
            }
        }

        private RelayCommand _UIpageSwitcher;
        public RelayCommand UIPageSwitcher
        {
            get
            {
                return _UIpageSwitcher ??
                  (_UIpageSwitcher = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      var dock = x as DockPanel;

                      switch (dock.Name)
                      {
                          case "CATEGORIES":
                              {
                                  CollapsedSomeDockPanel(ui.MENU);
                                  VisibleSomeDockPanel(ui.BAR);
                                  VisibleSomeDockPanel(dock);
                                  Page.Push(dock);
                                  ui.TIP.Visibility = Visibility.Visible;
                                  FillTIPforCategory();
                                  break;
                              }
                          case "LK":
                              {

                                  SaveScore();

                                  Properties.Settings.Default.Uname = "";
                                  Properties.Settings.Default.Upassword = "";
                                  Properties.Settings.Default.Save();

                                  var someDockPanel = Page.Peek();
                                  CollapsedSomeDockPanel(someDockPanel);
                                  VisibleSomeDockPanel(dock);
                                  Page.Push(dock);

                                  CUser.Name = "";
                                  CUser.Password = "";
                                  break;
                              }
                          case "SETTINGS":
                              {
                                  ui.BAR.Visibility = Visibility.Visible;
                                  ui.TIP.Visibility = Visibility.Collapsed;
                                  ui.NEXT.Visibility = Visibility.Collapsed;
                                  ui.GAMEBAR.Visibility = Visibility.Hidden;
                                  var someDockPanel = Page.Peek();
                                  CollapsedSomeDockPanel(someDockPanel);
                                  VisibleSomeDockPanel(dock);
                                  Page.Push(dock);
                                  break;
                              }
                          default: break;
                      }
                  }));
            }
        }

        private RelayCommand _UIopenGame;
        public RelayCommand UIOpenGame
        {
            get
            {
                return _UIopenGame ??
                  (_UIopenGame = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      var button = x as Button;

                      if (button.Name.ToString() == "customCategory")
                      {
                          CollapsedSomeDockPanel(ui.CATEGORIES);
                          ui.GAMEBAR.Visibility = Visibility.Hidden;
                          ui.NEXT.Visibility = Visibility.Visible;
                          ui.TIP.Visibility = Visibility.Collapsed;
                          VisibleSomeDockPanel(ui.CUSTOM);
                          Page.Push(ui.CUSTOM);
                      }
                      else
                      {
                          CurrentWord = "Загрузка . . .";
                          currentTable = button.Name.ToString();
                          
                          _task = Task.Factory.StartNew(new Action<object>(GetNewWord), currentTable);
                          
                          CollapsedSomeDockPanel(ui.CATEGORIES);
                          ui.GAMEBAR.Visibility = Visibility.Visible;
                          VisibleSomeDockPanel(ui.GAME);
                          Page.Push(ui.GAME);
                          FillTIPforGame();
                          OpenAllAlfabetSymbols();
                      }

                      if (CurrentWord.Contains('!'))
                      {
                          HiddenAllAlfabetSymbols();
                      }

                      hangmanIMG = "Resources/Images/1.png";
                      CurrentTableRus = button.Content.ToString();
                  }));
            }
        }


        private RelayCommand _UIgetCustomWord;
        public RelayCommand UIGetCustomWord
        {
            get
            {
                return _UIgetCustomWord ??
                  (_UIgetCustomWord = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      var button = x as Button;

                      CurrentWord = "Загрузка . . .";
                      currentTable = button.Name.ToString();
                      _task = Task.Factory.StartNew(new Action<object>(GetNewWord), currentTable);

                      CollapsedSomeDockPanel(ui.CUSTOM);

                      ui.GAMEBAR.Visibility = Visibility.Visible;
                      ui.NEXT.Visibility = Visibility.Collapsed;
                      ui.TIP.Visibility = Visibility.Visible;
                      
                      VisibleSomeDockPanel(ui.GAME);
                      Page.Push(ui.GAME);
                      OpenAllAlfabetSymbols();

                      CUser.Life = 7;
                      hangmanIMG = "Resources/Images/1.png";
                  }));
            }
        }



        private RelayCommand _openChar;
        public RelayCommand OpenChar
        {
            get
            {
                return _openChar ??
                  (_openChar = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      var ch = x as myChar;
                      if (!buttonsleep)
                      {
                          if (secretWord.Contains(ch.c))
                              openChar(ch.c);
                          else
                          {
                              CUser.Life--;
                              var j = (CUser.Life - 8) * -1;
                              hangmanIMG = "Resources/Images/" + j + ".png";
                          }
                          ch.visibility = Visibility.Hidden;
                          Rules();
                      }
                  }));
            }
        }

        private RelayCommand _fiftyFifty;
        public RelayCommand FiftyFifty
        {
            get
            {
                return _fiftyFifty ??
                  (_fiftyFifty = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      if (!buttonsleep)
                      {
                          int countVisibleChar = 0;
                          foreach (var c in Alphabet )
                          {
                              if (c.visibility == Visibility.Visible)
                                  countVisibleChar++;
                          }

                          var rnd = new Random();
                          for (int i = 0; i < countVisibleChar / 2;)
                          {
                              var j = rnd.Next() % 33;
                              if (!secretWord.Contains(Alphabet [j].c) && Alphabet [j].visibility != Visibility.Hidden)
                              {
                                  Alphabet [j].visibility = Visibility.Hidden;
                                  i++;
                              }

                          }

                          fiftyfifty = -3;
                      }
                  }, x =>
                  {
                      if (fiftyfifty < 0)
                          return false;
                      int countVisibleChar = 0;
                      int minC = 0;
                      foreach (var c in Alphabet )
                      {
                          if (c.visibility == Visibility.Visible)
                              countVisibleChar++;
                          if (secretWord.Contains(c.c))
                              minC++;
                      }
                      if ((countVisibleChar / 2) <= minC)
                          return false;

                      return true;
                  }));
            }
        }

        private RelayCommand _openRandomSymbol;
        public RelayCommand OpenRandomSymbol
        {
            get
            {
                return _openRandomSymbol ??
                  (_openRandomSymbol = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      if (!buttonsleep)
                      {
                          ORS();

                          ors = -2;
                          Rules();
                      }
                  }, x => {
                      if (ors < 0)
                          return false;

                      return true;
                  }));
            }
        }

        private void GetRatingTop10()
        {
            var client = new HttpClient();
            string url = "https://localhost:44335/api/RatingTop10";
            var response = client.Get(url);
            var rating = response.StaticBody<User[]>();
            ui.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                      new NextPrimeDelegateUsers(PrepareTableOfRating), rating);

        }
        private void PrepareTableOfRating(User[] rating)
        {
            ui.Loading.Visibility = Visibility.Collapsed;
            int i = 1;
            Rating.Clear();
            foreach (var u in rating)
            {
                u.place = i;
                Rating.Add(u);
                i++;
            }
        }


        private RelayCommand _UIopenRating;
        public RelayCommand UIOpenRating
        {
            get
            {
                return _UIopenRating ??
                  (_UIopenRating = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      ui.Loading.Visibility = Visibility.Visible;
                      _task = Task.Factory.StartNew(GetRatingTop10);

                      var someDockPanel = Page.Peek();

                      if (someDockPanel != ui.RATING)
                      {
                          ui.BAR.Visibility = Visibility.Visible;
                          ui.TIP.Visibility = Visibility.Hidden;
                          CollapsedSomeDockPanel(someDockPanel);
                          VisibleSomeDockPanel(ui.RATING);
                          Page.Push(ui.RATING);
                      }
                  }));
            }
        }

        private void RunAuthorized(object str)
        {
            var client = new HttpClient();
            string strs = (string)str;
            string url = "https://localhost:44335/api/" + strs;
            string data = JsonConvert.SerializeObject(CUser);
            var response = client.Post(url, data, HttpContentTypes.ApplicationJson);
            Error = response.StaticBody<string>();
            if (Error == "")
            {
                ui.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                                          new NextPrimeDelegate(isAuthorized));
            }
        }
        private void isAuthorized()
        {
            if (CUser.RememberMe)
            {
                Properties.Settings.Default.Uname = CUser.Name;
                Properties.Settings.Default.Upassword = CUser.Password;
                Properties.Settings.Default.Save();
            }
            var someDockPanel = Page.Peek();
            CollapsedSomeDockPanel(someDockPanel);
            VisibleSomeDockPanel(ui.MENU);
            Page.Push(ui.MENU);
        }
        private RelayCommand login;
        public RelayCommand LOGIN
        {
            get
            {
                return login ??
                  (login = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      CUser.Password = CUser.Password.Trim();
                      CUser.Name = CUser.Name.Trim();

                      Error = "Ожидаем  ответа  от  сервера";
                      object str = "Login";
                      _task = Task.Factory.StartNew(new Action<object>(RunAuthorized), str);
                  }));
            }
        }

        private RelayCommand registration;
        public RelayCommand Registration
        {
            get
            {
                return registration ??
                  (registration = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      CUser.Password = CUser.Password.Trim();
                      CUser.Name = CUser.Name.Trim();

                      Error = "Ожидаем  ответа  от  сервера";
                      object str = "Registration";
                      _task = Task.Factory.StartNew(new Action<object> (RunAuthorized), str);
                  }));
            }
        }

        private RelayCommand exit;
        public RelayCommand EXIT
        {
            get
            {
                return exit ??
                  (exit = new RelayCommand(x =>
                  {
                      MMP.chalkButtonSound();
                      SaveScore();
                      MMP.Close();
                      ui.Close();
                  }));
            }
        }
    }

    public class RelayCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }
    public class myChar : NPC
    {
        public int X { get; set; }
        public int Y { get; set; }
        public char c { get; set; }

        Visibility _visibility;
        public Visibility visibility { get { return _visibility; } set { _visibility = value; OnPropertyChanged(); } }
    }
    public class User : NPC
    {
        int _score;
        public int Score { get { return _score; } set { _score = value; OnPropertyChanged(); } }
        public int place { get; set; }
        public string Name { get; set; }
    }
    public class CurrentUser : User
    {
        int _life;
        public int Life { get { return _life; } set { _life = value; OnPropertyChanged(); } }
        string _password;
        public string Password { get { return _password; } set { _password = value; OnPropertyChanged(); } }

        bool _rememberMe;
        public bool RememberMe { get { return _rememberMe; } set { _rememberMe = value; OnPropertyChanged(); } }
        public CurrentUser()
        {
            Score = 0;
            Life = 7;
            Name = "";
            Password = "";
        }

    }
    public class MyCategory : NPC
    {
        bool _alife;
        int _length = 4;
        int _openSymbol;
        bool _beginVowel;
        bool _beginConsonant;
        bool _endVowel;
        bool _endConsonant;
        bool _man;
        bool _woman;
        bool _neuter;

        public bool Alife { get { return _alife; } set { _alife = value; OnPropertyChanged(); } }
        public int Length { get { return _length; } set { _length = value; OnPropertyChanged(); } }
        public int OpenSymbol { get { return _openSymbol; } set { _openSymbol = value; OnPropertyChanged(); } }
        public bool BeginVowel { get { return _beginVowel; } set { _beginVowel = value; OnPropertyChanged(); } }
        public bool BeginConsonant { get { return _beginConsonant; } set { _beginConsonant = value; OnPropertyChanged(); } }
        public bool EndVowel { get { return _endVowel; } set { _endVowel = value; OnPropertyChanged(); } }
        public bool EndConsonant { get { return _endConsonant; } set { _endConsonant = value; OnPropertyChanged(); } }
        public bool Man { get { return _man; } set { _man = value; OnPropertyChanged(); } }
        public bool Woman { get { return _woman; } set { _woman = value; OnPropertyChanged(); } }
        public bool Neuter { get { return _neuter; } set { _neuter = value; OnPropertyChanged(); } }
    }
    public class MyMediaPlayer : NPC
    {
        MediaPlayer mediaPlayer;
        MediaPlayer mediaPlayerFX;
        MediaPlayer mediaPlayerFXWinLose;
        private double _musicVolume = 50;
        private double _FXVolume = 50;
        public double musicVolume { get { return _musicVolume; } set { _musicVolume = value; mediaPlayer.Volume = _musicVolume / 100; OnPropertyChanged(); } }
        public double FXVolume { get { return _FXVolume; } set { _FXVolume = value; 
                                                                mediaPlayerFX.Volume = _FXVolume / 100;
                                                                mediaPlayerFXWinLose.Volume = mediaPlayerFX.Volume;
                                                                OnPropertyChanged(); } }
        public MyMediaPlayer()
        {
            mediaPlayer = new MediaPlayer();
            mediaPlayerFX = new MediaPlayer();
            mediaPlayerFXWinLose = new MediaPlayer();
            mediaPlayer.MediaEnded += new EventHandler(Media_Ended);
            mediaPlayer.Open(new Uri("Resources/Sounds/0.wav", UriKind.Relative));
            mediaPlayer.Play();
        }

        private void Media_Ended(object sender, EventArgs e)
        {
            mediaPlayer.Position = TimeSpan.Zero;
            mediaPlayer.Play();
        }

        public void chalkButtonSound()
        {
            var rnd = new Random();
            var i = rnd.Next() % 5 + 1;
            string str = "Resources/Sounds/" + i + ".wav";
            mediaPlayerFX.Open(new Uri(str, UriKind.Relative));
            mediaPlayerFX.Play();
        }
        public void winSound()
        {
            string str = "Resources/Sounds/win.wav";
            mediaPlayerFXWinLose.Open(new Uri(str, UriKind.Relative));
            mediaPlayerFXWinLose.Play();
        }
        public void loseSound()
        {
            string str = "Resources/Sounds/lose2.wav";
            mediaPlayerFXWinLose.Open(new Uri(str, UriKind.Relative));
            mediaPlayerFXWinLose.Play();
        }
        public void Close()
        {
            mediaPlayer.Close();
            mediaPlayerFX.Close();
            mediaPlayerFXWinLose.Close();
        }
    }
}
