﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HangmanGame
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var MVM = new MainViewModel(this);

            if (Properties.Settings.Default.Uname == "" ||
                Properties.Settings.Default.Uname == null ||
                Properties.Settings.Default.Upassword == "" ||
                Properties.Settings.Default.Upassword == null)
            {
                LK.Visibility = Visibility.Visible;
                MENU.Visibility = Visibility.Collapsed;
                MVM.Page.Push(this.LK);
            }
            else
            {
                MVM.CUser.Name = Properties.Settings.Default.Uname;
                MVM.CUser.Password = Properties.Settings.Default.Upassword;
                LK.Visibility = Visibility.Collapsed;
                MENU.Visibility = Visibility.Visible;
                MVM.Page.Push(this.MENU);
            }
            DataContext = MVM;
        }
    }
}
