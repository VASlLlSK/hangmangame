-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: hangman
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cities` (
  `id` int NOT NULL AUTO_INCREMENT,
  `word` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Баку'),(2,'Ереван'),(3,'Кабул'),(4,'Минск'),(5,'София'),(6,'Ватикан'),(7,'Лондон'),(8,'Ханой'),(9,'Каракас'),(10,'Берлин'),(11,'Афины'),(12,'Розо'),(13,'Каир'),(14,'Иерусалим'),(15,'Джакарда'),(16,'Тегеран'),(17,'Мадрид'),(18,'Гавана'),(19,'Бейрут'),(20,'Вильнюс'),(21,'Люксембрг'),(22,'Мехико'),(23,'Кишинев'),(24,'Монако'),(25,'Амстердам'),(26,'Осло'),(27,'Панама'),(28,'Варшава'),(29,'Лиссабон'),(30,'Москва'),(31,'Бухарест'),(32,'Пхеньян'),(33,'Дакар'),(34,'Белград'),(35,'Сингапур'),(36,'Братислава'),(37,'Вашингтон'),(38,'Душанбе'),(39,'Бангкок'),(40,'Тунис'),(41,'Одесса'),(42,'Севастополь'),(43,'Киев'),(44,'Новороссийск'),(45,'Волгоград'),(46,'Керчь'),(47,'Мурманск'),(48,'Смоленск'),(49,'Брест'),(50,'Ухань'),(51,'Турин'),(52,'Харбин'),(53,'Токио'),(54,'Тегеран'),(55,'Флоренция'),(56,'Тбилиси'),(57,'Ташкент'),(58,'Талин'),(59,'Роттердам'),(60,'Рига'),(61,'Пхукет'),(62,'Прага'),(63,'Орлеан'),(64,'Пекин'),(65,'Ницца'),(66,'Пекин'),(67,'Панама'),(68,'Неаполь'),(69,'Куршавель'),(70,'Краков'),(71,'Котор'),(72,'Львов'),(73,'Люблин'),(74,'Коломбо'),(75,'Колимбия'),(76,'Луганск'),(77,'Лозанна'),(78,'Кельн'),(79,'Катовице'),(80,'Лион'),(81,'Катания'),(82,'Карачи'),(83,'Канны'),(84,'Лейпциг'),(85,'Лахти'),(86,'Кандагар'),(87,'Ларнака'),(88,'Калькутта'),(89,'Кабул'),(90,'Лагос'),(91,'Дюссельдорф'),(92,'Дублин'),(93,'Дубай'),(94,'Горконг'),(95,'Джакарта'),(96,'Дамаск'),(97,'Гамбург'),(98,'Давос'),(99,'Афины'),(100,'Амстердам');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-04 19:28:11
