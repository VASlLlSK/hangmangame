-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: hangman
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sport`
--

DROP TABLE IF EXISTS `sport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sport` (
  `id` int NOT NULL AUTO_INCREMENT,
  `word` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sport`
--

LOCK TABLES `sport` WRITE;
/*!40000 ALTER TABLE `sport` DISABLE KEYS */;
INSERT INTO `sport` VALUES (1,'Автоспорт'),(2,'Айкидо'),(3,'Акробатика'),(4,'Альпинизм'),(5,'Армспорт'),(6,'Бадминтон'),(7,'Баскетбол'),(8,'Бейсбол'),(9,'Биатлон'),(10,'Бильярд'),(11,'Бобслей'),(12,'Бодибилдинг'),(13,'Бокс'),(14,'Борьба'),(15,'Боулинг'),(16,'Бочча'),(17,'Велоспорт'),(18,'Волейбол'),(19,'Гандбол'),(20,'Гимнастика'),(21,'Гольф'),(22,'Дайвинг'),(23,'Дартс'),(24,'Дзюдо'),(25,'Единоборства'),(26,'Йога'),(27,'Каратэ'),(28,'Картинг'),(29,'Керлинг'),(30,'Киберспорт'),(31,'Кикбоксинг'),(32,'Коньки'),(33,'Кудо'),(34,'Мотобол'),(35,'Мотоспорт'),(36,'Пауэрлифтинг'),(37,'Пейнтбол'),(38,'Плавание'),(39,'Покер'),(40,'Прыжки'),(41,'Радиоспорт'),(42,'Рафтиннг'),(43,'Регби'),(44,'Сани'),(45,'Сапсёрфинг'),(46,'Сёрфинг'),(47,'Скалолазание'),(48,'Сквош'),(49,'Скейтбординг'),(50,'Скелетон'),(51,'Сноуборд'),(52,'Софтбол'),(53,'Сумо'),(54,'Теннис'),(55,'Триатлон'),(56,'Тхэквондо'),(57,'Фехтование'),(58,'Фристайл'),(59,'Футбол'),(60,'Хоккей'),(61,'Хортинг'),(62,'Чирлидинг'),(63,'Шахматы'),(64,'Шашки');
/*!40000 ALTER TABLE `sport` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-04 19:28:09
