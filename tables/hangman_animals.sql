-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: hangman
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `animals`
--

DROP TABLE IF EXISTS `animals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `animals` (
  `id` int NOT NULL AUTO_INCREMENT,
  `word` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `animals`
--

LOCK TABLES `animals` WRITE;
/*!40000 ALTER TABLE `animals` DISABLE KEYS */;
INSERT INTO `animals` VALUES (1,'Акула'),(2,'Антилопа'),(3,'Бандикут'),(4,'Баран'),(5,'Барс'),(6,'Белка'),(7,'Бизон'),(8,'Бородавочник'),(9,'Буйвол'),(10,'Бурундук'),(11,'Верблюд'),(12,'Волк'),(13,'Выдра'),(14,'Выхухоль'),(15,'Гепард'),(16,'Гиена'),(17,'Гиппопотам'),(18,'Гиракс'),(19,'Горилла'),(20,'Горностай'),(21,'Дельфин'),(22,'Дзерен'),(23,'Дикобраз'),(24,'Ехидна'),(25,'Жираф'),(26,'Зебра'),(27,'Зубр'),(28,'Кабарга'),(29,'Каракал'),(30,'Карибу'),(31,'Кенгуру'),(32,'Коала'),(33,'Коза'),(34,'Койот'),(35,'Корова'),(36,'Косуля'),(37,'Кошка'),(38,'Крокодил'),(39,'Кролик'),(40,'Кулан'),(41,'Куница'),(42,'Лама'),(43,'Лемур'),(44,'Леопард'),(45,'Лисица'),(46,'Лось'),(47,'Лошадь'),(48,'Львица'),(49,'Мангуст'),(50,'Манул'),(51,'Медведь'),(52,'Морж'),(53,'Муравьед'),(54,'Нарвал'),(55,'Нерпа'),(56,'Норка'),(57,'Носорог'),(58,'Овца'),(59,'Олень'),(60,'Орангутанг'),(61,'Осел'),(62,'Оцелот'),(63,'Песец'),(64,'Пума'),(65,'Росомаха'),(66,'Рысь'),(67,'Свинья'),(68,'Сервал'),(69,'Слон'),(70,'Собака'),(71,'Суслик'),(72,'Тигр'),(73,'Тушканчик'),(74,'Утконос'),(75,'Хомяк'),(76,'Хорек'),(77,'Черепаха'),(78,'Шакал');
/*!40000 ALTER TABLE `animals` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-04 19:28:10
