-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: hangman
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `id` int NOT NULL AUTO_INCREMENT,
  `word` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Австралия'),(2,'Австрия'),(3,'Азербайджан'),(4,'Албан'),(5,'Алжир'),(6,'Англия'),(7,'Ангола'),(8,'Андорра'),(9,'Аргентина'),(10,'Армения'),(11,'Аруба'),(12,'Афганистан'),(13,'Бангладеш'),(14,'Барбадос'),(15,'Беларусь'),(16,'Бельгия'),(17,'Бенин'),(18,'Болгария'),(19,'Боливия'),(20,'Ботсвана'),(21,'Бутан'),(22,'Ватикан'),(23,'Венгрия'),(24,'Венесуэла'),(25,'Вьетнам'),(26,'Габон'),(27,'Гаити'),(28,'Гваделупа'),(29,'Германия'),(30,'Гондурас'),(31,'Гренада'),(32,'Греция'),(33,'Грузия'),(34,'Дания'),(35,'Джибути'),(36,'Доминика'),(37,'Египет'),(38,'Замбия'),(39,'Зимбабве'),(40,'Израиль'),(41,'Индия'),(42,'Индонезия'),(43,'Иордания'),(44,'Ирландия'),(45,'Испания'),(46,'Казахстан'),(47,'Камерун'),(48,'Канада'),(49,'Киргизия'),(50,'Колумбия'),(51,'Ливия'),(52,'Люксембург'),(53,'Малави'),(54,'Мали'),(55,'Мальдивы'),(56,'Мальта'),(57,'Мексика'),(58,'Мозамбик'),(59,'Молдавия'),(60,'Монако'),(61,'Монголия'),(62,'Намибия'),(63,'Непал'),(64,'Нигер'),(65,'Нигерия'),(66,'Никарагуа'),(67,'Норвегия'),(68,'Пакистан'),(69,'Палистина'),(70,'Панама'),(71,'Польша'),(72,'Португалия'),(73,'Россия'),(74,'Румыния'),(75,'Румыния'),(76,'Сальвадор'),(77,'Сенегал'),(78,'Сербия'),(79,'Сингапур'),(80,'Таджикистан'),(81,'Тайланд'),(82,'Тунис'),(83,'Украина'),(84,'Уругвай'),(85,'Фиджи'),(86,'Филиппины'),(87,'Франция'),(88,'Хорватия'),(89,'Черногория'),(90,'Чехия'),(91,'Швейцария'),(92,'Швеция'),(93,'Эквадор'),(94,'Эстония'),(95,'Эфиопия'),(96,'Ямайка'),(97,'Япония');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-04 19:28:11
