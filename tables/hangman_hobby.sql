-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: hangman
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hobby`
--

DROP TABLE IF EXISTS `hobby`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hobby` (
  `id` int NOT NULL AUTO_INCREMENT,
  `word` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hobby`
--

LOCK TABLES `hobby` WRITE;
/*!40000 ALTER TABLE `hobby` DISABLE KEYS */;
INSERT INTO `hobby` VALUES (1,'Археология'),(2,'Астрономия'),(3,'Аэробика'),(4,'Аэрография'),(5,'Бадминтон'),(6,'Балет'),(7,'Бильярд'),(8,'Блогерство'),(9,'Бодиарт'),(10,'Бонсай'),(11,'Боулинг'),(12,'Велосипед'),(13,'Видеомонтаж'),(14,'Выжигание'),(15,'Выставки'),(16,'Вышивание'),(17,'Вязание'),(18,'Гербарий'),(19,'Головоломки'),(20,'Граффити'),(21,'Дайвинг'),(22,'Дартс'),(23,'Диггерство'),(24,'Жонглирование'),(25,'Игры'),(26,'Икебана'),(27,'Йога'),(28,'Кайтинг'),(29,'Каллиграфия'),(30,'Караоке'),(31,'Картинг'),(32,'Кладоискательство'),(33,'Книги'),(34,'Коллекционирование'),(35,'Коньки'),(36,'Косплей'),(37,'Кроссворды'),(38,'Кулинария'),(39,'Лазертаг'),(40,'Лепка'),(41,'Лыжи'),(42,'Массаж'),(43,'Моделирование'),(44,'Музеи'),(45,'Мыловарение'),(46,'Оригами'),(47,'Охота'),(48,'Паззлы'),(49,'Паркур'),(50,'Пейнтбол'),(51,'Петанк'),(52,'Пикап'),(53,'Пилатес'),(54,'Пирсинг'),(55,'Плавание'),(56,'Плетение'),(57,'Предпринимательство'),(58,'Программирование'),(59,'Путешествия'),(60,'Пчеловодство'),(61,'Радиовещание'),(62,'Рисование'),(63,'Робототехника'),(64,'Ролики'),(65,'Рыбалка'),(66,'Серфинг'),(67,'Скейтборд'),(68,'Скрапбукинг'),(69,'Сноуборд'),(70,'Стекловыдувание'),(71,'Страйкбол'),(72,'Стрельба'),(73,'Танцы'),(74,'Татуировки'),(75,'Театр'),(76,'Теннис'),(77,'Тренировки'),(78,'Фейерверки'),(79,'Фокусы'),(80,'Фотография'),(81,'Фотокниги'),(82,'Футбол'),(83,'Хэндмэйт'),(84,'Шитье');
/*!40000 ALTER TABLE `hobby` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-04 19:28:11
