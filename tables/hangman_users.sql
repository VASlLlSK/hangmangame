-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: hangman
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) DEFAULT NULL,
  `Password` varchar(45) DEFAULT NULL,
  `PassedWords` varchar(4096) DEFAULT NULL,
  `Score` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','Admin','{\"animals\":[16],\"cities\":[],\"country\":[],\"hobby\":[],\"nouns\":[],\"sport\":[]}',3),(2,'VASILISK','54321','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',0),(3,'Sasha','12345','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',0),(4,'изуз','aye228','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',0),(5,'asd','asd','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',0),(6,'Татка','5555','{\"animals\":[],\"cities\":[83],\"country\":[],\"hobby\":[],\"nouns\":[],\"sport\":[]}',2),(7,'Женя','12345','{\"animals\":[],\"cities\":[],\"country\":[54],\"hobby\":[],\"nouns\":[],\"sport\":[7,29,38,41]}',2),(8,'я','я','{\"animals\":[],\"cities\":[],\"country\":[],\"hobby\":[9],\"nouns\":[],\"sport\":[]}',13),(9,'qwerty','qwerty','{\"animals\":[],\"cities\":[98,6],\"country\":[],\"hobby\":[],\"nouns\":[],\"sport\":[]}',1),(10,'123','123','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',0),(11,'ertf','jgh','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',0),(12,'hjkl','0poikj','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',0),(13,'1','1','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',6),(14,'rt','rty','{\"animals\": [], \"cities\": [], \"country\": [], \"hobby\": [], \"nouns\": [], \"sport\": []}',0),(15,'фывль','вяа','{\"animals\":[],\"cities\":[45],\"country\":[51],\"hobby\":[58],\"nouns\":[21734],\"sport\":[5]}',0),(16,'ацуа','ыва','{\"animals\":[],\"cities\":[45],\"country\":[],\"hobby\":[],\"nouns\":[],\"sport\":[]}',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-04 19:28:10
